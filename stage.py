# coding: utf-8
"""
Ce script python fonctionne sans modification. Il affiche le nombre de chlorydrates dans le CSV 'molecules.csv'
"""

"""
Le csv dans ce dossier a été généré avec la requête SQL suivante :

SELECT distinct p_name FROM compo
WHERE p_name NOT LIKE '%HOMÉO%'
"""

counter = []

try:
    with open(r"molecules.csv", "r", encoding="utf-8") as fp:  # ouverture du fichier csv
        [counter.append(1) for item in fp.read().split('\n')
         if "chlorhydrate" in item.lower()]

        print(len(counter))
        input()

except FileNotFoundError:
    path = input(
        "chemin de csv non trouvé. Glissez déposez le fichier sur la fenêtre svp :\n")
    try:
        with open(path, encoding="utf-8") as fp:
            [counter.append(1) for item in fp.read().split('\n')
             if "chlorhydrate" in item.lower()]

        print(len(counter))
        input()

    except Exception as errorname:
        print(f"{errorname} : le fichier csv n'a pas été lu.")
